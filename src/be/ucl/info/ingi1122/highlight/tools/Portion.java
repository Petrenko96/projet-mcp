package be.ucl.info.ingi1122.highlight.tools;


public class Portion
{
    
    public int begin;
    public int end;

    /**
     * Constructor for objects of class Portion
     */
    public Portion(int begin, int end)
    {
        this.begin = begin;
        this.end = end;
    }

    public int getBegin(){
        return this.begin;
    }
    
    public int getEnd(){
        return this.end;
    }
    
    public void setBegin(int begin){
        this.begin = begin;
    }
    
    public void setEnd(int end){
        this.end = end;
    }
    
    public int getLength(){
        return this.end - this.begin;
    }
    
    public String toString() {
    	return "("+begin+";"+end+")";
    }
}
