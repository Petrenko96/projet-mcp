package be.ucl.info.ingi1122.highlight.tools;

public class Tools {

	public static Portion[] quoiSurligner(char[] txt, char[][] mots) {
    	PortionSet p = new PortionSet(txt);
    	boolean ok;
    	for(int j = 0 ; j < txt.length ; j++) {
    		for(int i = 0 ;  i < mots.length ; i++) {
    			ok = true;
    			for(int k = 0 ; k < mots[i].length && ok; k++) {
    				if(j+k >= txt.length || txt[j+k] != mots[i][k])
    					ok = false;
    			}
    			if(ok) {
    				p.Add(new Portion(j, j+mots[i].length));
    			}
    		}
    	}
    	return p.toArray();
    }
	
	 public static boolean match(char[] txt, char[] mots){
	        for(int i=0;i<=txt.length-mots.length;i++){
	            int j;
	            for(j=0;j<mots.length && txt[i+j]==mots[j];j++){
	            }
	            if (j==mots.length){
	                return true;
	            }
	        }
	        return false;
	    }
	    
	    public static boolean correspond(char[] txt, char[][] mots){
	        for(int i=0; i<mots.length;i++){
	            if(!match(txt, mots[i])){
	                return false;
	            }
	        }
	        return true;
	    }
}