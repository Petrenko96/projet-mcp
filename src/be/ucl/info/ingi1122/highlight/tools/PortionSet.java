package be.ucl.info.ingi1122.highlight.tools;

import java.util.ArrayList;

public class PortionSet
{
    public char[] texte;
    public ArrayList<Portion> portions;
    public int i;
    
    public PortionSet(char[] texte){
        this.texte = texte;
        this.portions = new ArrayList<Portion>();
        this.i=0;
    }
    
    public void add(Portion P){
    	boolean toAdd = true;
       for(Portion p : portions) {
    	   if(P.getBegin() >= p.getBegin() && P.getBegin() <= p.getEnd()) {
    		   p.setEnd(Math.max(P.getEnd(), p.getEnd()));
    		   P = p;
    		   toAdd = false;
    	   }
    	   else if(P.getEnd() >= p.getBegin() && P.getEnd() <= p.getEnd()) {
    		   p.setBegin(Math.max(P.getBegin(), p.getBegin()));
    		   P = p;
    		   toAdd = false;
    	   }
       }
       if(toAdd) {
    	   portions.add(P);
       }
    }
    
    public Portion remove(){
        Portion port = portions.get(i);
        portions.remove(i);
        this.i=i++;
        return port;
    }
    
    public int size(){
        return portions.size();
    }
    
    public boolean contains(Portion port){
        return portions.contains(port);
    }
    
    public Portion[] toArray() {
    	Portion p[] = new Portion[portions.size()];
    	portions.toArray(p);
    	return p;
    }
    
    public String toString() {
    	String j = "";
    	for(Portion p : portions) {
    		j += " ";
    		j+=p;
    	}
    	return j;
    }
}